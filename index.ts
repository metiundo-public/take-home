// Please use axios for your API requests
import axios from 'axios';
import { ElectricityReading, MeteringPoint, LoginResponse } from './types';

// Find the docs at https://api.metiundo.de/v1/docs
const baseUrl = "https://api.metiundo.de/v1";

